package main

import (
	"os"
	"time"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Convert []struct {
		From_id     uint    `yaml:"from_id"`
		From_amount float32 `yaml:"from_amount"`
		To_id       uint    `yaml:"to_id"`
		To_amount   float32 `yaml:"to_amount"`
	} `yaml:"convert"`

	From struct {
		Email     string `yaml:"email"`
		Password  string `yaml:"password"`
		Smtp_addr string `yaml:"smtp_addr"`
		Smtp_host string `yaml:"smtp_host"`
	} `yaml:"from_email"`

	To []string `yaml:"to_email"`

	Sleep time.Duration `yaml:"check_time"`
	Reset time.Duration `yaml:"check_reset"`
}

func load_config(file_name string) Config {
	// Open file.
	f, err := os.Open(file_name)
	if err != nil {
		panic(err)
	}
	defer f.Close()

	var cfg Config

	// Convert file to struct.
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		panic(err)
	}

	return cfg
}
