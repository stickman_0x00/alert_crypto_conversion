package main

import (
	"fmt"
	"log"
	"net/smtp"
	"strings"
)

func send_email(from, to string, amount float32) {
	// Build message
	// From:
	msg := "From: " + config.From.Email + "\n"
	// To:
	msg += "To: " + strings.Join(config.To, ",") + "\n"
	// Subject
	msg += fmt.Sprintf("Subject: Go see -> %s -> %s: %f\n\n", from, to, amount)

	// Send email
	err := smtp.SendMail(config.From.Smtp_addr,
		smtp.PlainAuth("", config.From.Email, config.From.Password, config.From.Smtp_host),
		config.From.Email,
		config.To,
		[]byte(msg),
	)

	if err != nil {
		log.Printf("smtp error: %s", err)
	}
}
