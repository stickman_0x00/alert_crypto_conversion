package main

import (
	"fmt"
	"log"
	"sync"
	"time"
)

const (
	CONFIG_FILE = "./config.yml"
)

var (
	config  = load_config(CONFIG_FILE)
	alerted = make(map[string]time.Time)
)

func run(wg *sync.WaitGroup, from_amount, to_amount float32, from_id, to_id uint) {
	defer wg.Done()

	id := fmt.Sprintf("%f%f%d%d", from_amount, to_amount, from_id, to_id)
	// Check if pair was alerted in the last X hours.
	if _, exist := alerted[id]; exist {
		return
	}

	// Get result
	result := get_conversion(from_amount, from_id, to_id)
	// check status code
	if result.Status.Error_code != "0" {
		log.Printf("Error_code: from: %d, to: %d", from_id, to_id)
		return
	}

	if len(result.Data.Quote) == 0 {
		log.Printf("No quote: from: %d, to: %d", from_id, to_id)
		return
	}

	if result.Data.Quote[0].Price < to_amount {
		// Exit if percentage is not bigger (TARGET)
		return
	}
	fmt.Println("Enough", result)

	// PUMP
	// Alert
	log.Println("Alert: ", result.Data.Symbol, result.Data.Quote[0].Symbol)
	send_email(result.Data.Symbol, result.Data.Quote[0].Symbol, result.Data.Quote[0].Price)

	// Save alert
	alerted[id] = time.Now()
}

func main() {
	wg := sync.WaitGroup{}

	for {
		for _, convert := range config.Convert {
			wg.Add(1)
			run(&wg,
				convert.From_amount, convert.To_amount,
				convert.From_id, convert.To_id,
			)
		}

		// Wait for each convertion.
		wg.Wait()

		// Wait for next check
		time.Sleep(config.Sleep * time.Minute)

		// Reset alerted pairs
		for key, last_alert := range alerted {
			if time.Since(last_alert) > config.Reset*time.Minute {
				// If have passed X hours from last alert.
				log.Println("Reset: ", key)
				delete(alerted, key)
			}
		}
	}
}
