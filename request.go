package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

// Example: GET https://api.coinmarketcap.com/data-api/v3/tools/price-conversion?amount=7&convert_id=1&id=1437
type response_json struct {
	Data struct {
		Symbol string `json:"symbol"`
		Quote  []struct {
			Symbol string  `json:"symbol"`
			Price  float32 `json:"price"`
		} `json:"quote"`
	} `json:"data"`
	Status struct {
		Error_code string `json:"error_code"`
	}
}

func get_conversion(from_amount float32, from_id, to_id uint) response_json {
	var r response_json

	// get pair information
	url := fmt.Sprintf("https://api.coinmarketcap.com/data-api/v3/tools/price-conversion?amount=%f&convert_id=%d&id=%d",
		from_amount,
		to_id,
		from_id,
	)
	// fmt.Println(url)
	response, err := http.Get(url)
	if err != nil {
		log.Println("Request:", err)
		return r
	}

	if response.StatusCode != http.StatusOK {
		log.Printf("Failed: from: %d, to: %d", from_id, to_id)
		return r
	}

	// If request is successful convert json to struct
	decoder := json.NewDecoder(response.Body)

	if err = decoder.Decode(&r); err != nil {
		log.Println("Decode error:", err)
		return r
	}

	return r
}
